# Hypixel Ranked Seasons

All Hypixel Ranked seasons in JSON.

### Schemas

#### Season

|     Property    |      Type     |                                  Description                                 |
|:---------------:|:-------------:|:----------------------------------------------------------------------------:|
|    **number**   |      Int      |                           Season number, 1-indexed                           |
| **hiddenInAPI** |    Boolean    | Indicates whether Hypixel returns rating/position for a player in API or not |
| **leaderboard** | List\<Player> |                              Stored leaderboard                              |

##### Player

|   Property   |  Type  |   Description  |
|:------------:|:------:|:--------------:|
|   **uuid**   | String | UUID, undashed |
|   **name**   | String |      Name      |
|  **rating**  |   Int  |     Rating     |
| **position** |   Int  |    Position    |

## Contributing

You are free to contribute in any way.

##### Incomplete Leaderboards

* Season **4**: missing ratings for #2-9; missing #10
* Season **6**: completely unknown
* Season **9**: missing #10
* Season **11**: missing #1-4
* Season **15**: completely unknown
* Season **34**: missing rating for #2
* Season **35**: missing #5; missing #9

## License

No.
